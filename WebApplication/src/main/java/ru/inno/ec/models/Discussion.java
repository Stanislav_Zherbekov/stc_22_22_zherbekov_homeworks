package ru.inno.ec.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity

public class Discussion {


    public enum State {
        READY, NOT_READY, DELETED;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String theme;
    private LocalDate start_date;


    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    @ManyToMany(mappedBy = "discussion", fetch = FetchType.EAGER)
    private Set<User> account;
    @Enumerated(value = EnumType.STRING)
    private State state;

}
