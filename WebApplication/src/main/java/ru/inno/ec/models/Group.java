package ru.inno.ec.models;
import javax.persistence.*;
import lombok.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"discussion", "account"})
@Entity
@Table(name = "сommunity")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(length = 1000)
    private String description;
    @Column(length = 300)
    private String questions;


    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
    private Set<Discussion> discussion;

    @ManyToMany(mappedBy = "group", fetch = FetchType.EAGER)
    private Set<User> account;
}
