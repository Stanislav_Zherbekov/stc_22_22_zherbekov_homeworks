package ru.inno.ec.services;

import ru.inno.ec.dto.DiscussionForm;

import ru.inno.ec.models.Discussion;

import ru.inno.ec.models.User;


import java.util.List;

public interface DiscussionsService {

    void addUserToDiscussion(Long discussionId, Long accountId);

    void addDiscussion(DiscussionForm discussion);

    void deleteDiscussion(Long discussionId);

    Discussion getDiscussion(Long discussionId);

    public List<User> getInDiscusionUsers(Long discussionId);

    List<Discussion> getAllDiscussion();

    List<User> getNotInDiscussionUsers(Long discussionId);
}
