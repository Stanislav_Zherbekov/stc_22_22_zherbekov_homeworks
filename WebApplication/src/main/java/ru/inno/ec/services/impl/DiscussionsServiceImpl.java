package ru.inno.ec.services.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.DiscussionForm;
import ru.inno.ec.models.Discussion;
import ru.inno.ec.models.User;
import ru.inno.ec.repositories.DiscussionsRepository;
import ru.inno.ec.repositories.UsersRepository;
import ru.inno.ec.services.DiscussionsService;
import java.util.List;

@AllArgsConstructor
@Service
public class DiscussionsServiceImpl implements DiscussionsService {

    private final UsersRepository usersRepository;
    private final DiscussionsRepository discussionsRepository;

    @Override
    public void addUserToDiscussion(Long discussionId, Long accountId) {
        Discussion discussion = discussionsRepository.findById(discussionId).orElseThrow();
        User user = usersRepository.findById(accountId).orElseThrow();

        user.getDiscussion().add(discussion);

        usersRepository.save(user);
    }

    @Override
    public void addDiscussion(DiscussionForm discussion) {
        Discussion newDiscussion = Discussion.builder()
                .title(discussion.getTitle())
                .theme(discussion.getTheme())
                .start_date(discussion.getStart_date())
                .build();
        discussionsRepository.save(newDiscussion);

    }

    @Override
    public void deleteDiscussion(Long discussionId) {
        Discussion discussion = discussionsRepository.findById(discussionId).orElseThrow();
        discussion.setState(Discussion.State.DELETED);
        discussionsRepository.save(discussion);

    }

    @Override
    public Discussion getDiscussion(Long discussionId) {
        return discussionsRepository.findById(discussionId).orElseThrow();
    }

    @Override
    public List<User> getInDiscusionUsers(Long discussionId) {
        Discussion discussion = discussionsRepository.findById(discussionId).orElseThrow();
        return usersRepository.findAllByDiscussionContains(discussion);
    }


    @Override
    public List<Discussion> getAllDiscussion() {
        return discussionsRepository.findAll();
    }

    @Override
    public List<User> getNotInDiscussionUsers(Long discussionId) {
          Discussion discussion = discussionsRepository.findById(discussionId).orElseThrow();
        return usersRepository.findAllByDiscussionContainsAndState(discussion, User.State.CONFIRMED);
    }
}
