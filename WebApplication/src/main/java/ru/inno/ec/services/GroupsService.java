package ru.inno.ec.services;

import ru.inno.ec.dto.GroupForm;
import ru.inno.ec.models.Group;
import ru.inno.ec.models.User;

import java.util.List;

public interface GroupsService {
    void addUserToGroup(Long groupId, Long accountId);

    Group getGroup(Long groupId);

    List<Group> getAllGroup();

    List<User> getNotInGroupUsers(Long groupId);

    List<User> getInGroupUsers(Long groupId);

    void addGroup(GroupForm group);

    void addDiscussionToGroup(Long discussionId, Long groupId);
}
