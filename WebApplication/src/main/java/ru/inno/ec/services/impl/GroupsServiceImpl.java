package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.GroupForm;
import ru.inno.ec.models.Discussion;
import ru.inno.ec.models.Group;
import ru.inno.ec.models.User;
import ru.inno.ec.repositories.DiscussionsRepository;
import ru.inno.ec.repositories.GroupsRepository;
import ru.inno.ec.repositories.UsersRepository;
import ru.inno.ec.services.GroupsService;
import java.util.List;

@RequiredArgsConstructor
@Service
public class GroupsServiceImpl implements GroupsService {

    private final GroupsRepository groupsRepository;
    private final UsersRepository usersRepository;

    private final DiscussionsRepository discussionsRepository;

    @Override
    public void addUserToGroup(Long groupId, Long accountId) {
        Group group = groupsRepository.findById(groupId).orElseThrow();
        User user = usersRepository.findById(accountId).orElseThrow();

        user.getGroup().add(group);

        usersRepository.save(user);
    }

    @Override
    public Group getGroup(Long groupId) {
        return groupsRepository.findById(groupId).orElseThrow();
    }

    @Override
    public List<Group> getAllGroup() {
        return groupsRepository.findAll();
    }

    @Override
    public List<User> getNotInGroupUsers(Long groupId) {
        Group group = groupsRepository.findById(groupId).orElseThrow();
        return usersRepository.findAllByGroupNotContainsAndState(group, User.State.CONFIRMED);
    }

    @Override
    public List<User> getInGroupUsers(Long groupId) {
        Group group = groupsRepository.findById(groupId).orElseThrow();
        return usersRepository.findAllByGroupContains(group);
    }

    @Override
    public void addGroup(GroupForm group) {
        Group newGroup = Group.builder()
                .title(group.getTitle())
                .description(group.getDescription())
                .questions(group.getQuestions())
                .build();
        groupsRepository.save(newGroup);

    }

    @Override
    public void addDiscussionToGroup(Long DiscussionId, Long groupId) {
        Group group = groupsRepository.findById(groupId).orElseThrow();
        Discussion discussion = discussionsRepository.findById(DiscussionId).orElseThrow();
        group.getDiscussion().add(discussion);
        discussion.setGroup(group);
        groupsRepository.save(group);

    }
}
