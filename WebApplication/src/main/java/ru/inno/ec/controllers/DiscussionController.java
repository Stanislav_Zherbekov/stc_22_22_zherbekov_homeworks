package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.ec.dto.DiscussionForm;
import ru.inno.ec.security.details.CustomUserDetails;
import ru.inno.ec.services.DiscussionsService;
@RequiredArgsConstructor
@Controller
@RequestMapping("/discussions")
public class DiscussionController {
    private final DiscussionsService discussionsService;

    @GetMapping
    public String getDiscussionsPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("discussions", discussionsService.getAllDiscussion());
        return "discussions/discussions_page";
    }

    @PostMapping("/{discussion-id}/users")
    public String addUserToDiscussion(@PathVariable("discussion-id") Long discussionId,
                                 @RequestParam("user-id") Long userId) {
        discussionsService.addUserToDiscussion(discussionId, userId);
        return "redirect:/discussion/" + discussionId;

    }
    @GetMapping("/{discussion-id}")
    public String getDiscussionPage(@PathVariable("discussion-id") Long discussionId, Model model) {
        model.addAttribute("discussion", discussionsService.getDiscussion(discussionId));
        model.addAttribute("inDiscussionUser", discussionsService.getInDiscusionUsers(discussionId));
        model.addAttribute("notInDiscussionUser", discussionsService.getNotInDiscussionUsers(discussionId));
        return "discussions/discussion_page";
    }
    @PostMapping()
    public String AddDiscussion(DiscussionForm discussion) {
        discussionsService.addDiscussion(discussion);
        return "redirect:/discussions/";
    }
    @GetMapping("/discussions/{discussion-id}/delete")
    public String updateDiscussion(@PathVariable("discussion-id") Long discussionId) {
        discussionsService.deleteDiscussion(discussionId);
        return "redirect:/discussions/";
    }


}
