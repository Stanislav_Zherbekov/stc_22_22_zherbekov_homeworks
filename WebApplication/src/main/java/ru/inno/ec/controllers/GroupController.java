package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.ec.dto.GroupForm;

import ru.inno.ec.security.details.CustomUserDetails;
import ru.inno.ec.services.GroupsService;


@RequiredArgsConstructor
@Controller
@RequestMapping("/groups")
public class GroupController {

    private final GroupsService groupsService;


    @GetMapping
    public String getGroupsPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("groups", groupsService.getAllGroup());
        return "groups/groups_page";
    }


    @PostMapping("/{group-id}/users")
    public String addUserToGroup(@PathVariable("group-id") Long groupId,
                                 @RequestParam("user-id") Long userId) {
        groupsService.addUserToGroup(groupId, userId);
        return "redirect:/groups/" + groupId;
    }

    @GetMapping("/{group-id}")
    public String getGroupPage(@PathVariable("group-id") Long groupId, Model model) {
        model.addAttribute("group", groupsService.getGroup(groupId));
        model.addAttribute("notInGroupUsers", groupsService.getNotInGroupUsers(groupId));
        model.addAttribute("inGroupUsers", groupsService.getInGroupUsers(groupId));
        return "groups/group_page";
    }
    @PostMapping()
    public String AddGroup(GroupForm group){
        groupsService.addGroup(group);
        return "redirect:/groups/";
    }
    @PostMapping("/groups/{group-id}/discussions")
    public String addDiscussionToGroup(@PathVariable("group-id") Long groupId,
                                    @RequestParam("discussion-id") Long discussionId) {
        groupsService.addDiscussionToGroup(discussionId, groupId);
        return "redirect:/groups/{group-id}";
    }

}
