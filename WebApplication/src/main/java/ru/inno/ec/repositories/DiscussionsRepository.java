package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Discussion;
import ru.inno.ec.models.User;

import java.util.List;


public interface DiscussionsRepository extends JpaRepository<Discussion, Long> {
    }
