package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.inno.ec.models.Discussion;
import ru.inno.ec.models.Group;
import ru.inno.ec.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllByGroupNotContainsAndState(Group group, User.State state);

    List<User> findAllByGroupContains(Group group);

    List<User> findAllByDiscussionContainsAndState(Discussion discussion, User.State state);
    Optional<User> findByEmail(String email);

    List<User> findAllByDiscussionContains(Discussion discussion);


//    List<User> findAllByDiscussionContains(Discussion discussion);
}
