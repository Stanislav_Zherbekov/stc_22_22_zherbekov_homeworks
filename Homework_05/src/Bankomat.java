public class Bankomat {
    // сумма оставшихся денег в банкомате
    private int lastsum;
    // максимальная сумма к выдачи
    private int maxsum;
    //  Максимальный объем денег
    private int maxvalue;
    //  Количество проведенных операций
    private int countOperation;

    Bankomat(int lastsum, int maxsum, int maxvalue) {
        this.maxvalue = maxvalue;
        this.lastsum = lastsum;
        if (lastsum >= maxsum && lastsum <= maxvalue) {
            this.maxsum = maxsum;
        } else {
            this.maxsum = lastsum;
        }
    }

    public int givMoney(int sumMoney) {
        if (sumMoney <= this.lastsum && sumMoney <= this.maxsum) {
            this.lastsum -= sumMoney;
            this.countOperation++;
            return sumMoney;
        }


        if (sumMoney > lastsum) {
            System.out.println("В банкомате недостаточно средств");
        }
        if (sumMoney <= this.lastsum && sumMoney > this.maxsum) {
            System.out.println("Превышен максимальный лимит выдачи наличных!");
        }
        return 0;

    }

    public int putMoney(int money) {
        if (this.maxvalue > this.lastsum + money) {
            lastsum += money;
            this.countOperation++;
            return 0;
        }

        else{
            return ((lastsum + money) - maxvalue);
        }

    }

    public int getCountOperation() {
        return countOperation;
    }
}




