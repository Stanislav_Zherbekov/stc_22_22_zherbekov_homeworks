import java.util.Scanner;

public class Homework_04 {
    public static int sumInRange(int from, int to) {
        int sum = 0;
        if (from <= to) {
            for (int i = from; i <= to; i++) {
                sum += i;
            }
        } else {
            return -1;
        }
        return sum;
    }

    public static void printEvenInRage(int from, int to) {
        for (int i = from; i <= to; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите начальное число");
        int mumberStart = scanner.nextInt();
        System.out.println("Введите конечное число");
        int endnumber = scanner.nextInt();
        int sum = sumInRange(mumberStart, endnumber);
        System.out.println(sum);
        printEvenInRage(scanner.nextInt(), scanner.nextInt());

    }
}