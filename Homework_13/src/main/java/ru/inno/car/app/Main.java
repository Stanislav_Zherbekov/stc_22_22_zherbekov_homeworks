package ru.inno.cars.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.cars.models.Car;
import ru.inno.cars.repositiry.CarRepository;
import ru.inno.cars.repositiry.CarRepositoryJdbcImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

@Parameters(separators = "=")

public class Main {
    @Parameter(names = {"-action"})
    private String action;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Main main = new Main();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        Properties dbProperties = new Properties();
        try {
            dbProperties.load(new BufferedReader(new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));
        CarRepository carRepository = new CarRepositoryJdbcImpl(dataSource);
        if ("read".equals(main.action)) {
            List<Car> carsFromDb = carRepository.findAll();
            for (Car car : carsFromDb) {
                System.out.println(car);
            }
        } else if ("write".equals(main.action)) {
            while (true) {
                System.out.println("Введите информацию о машине:");
                System.out.println("Введите модель машины:");
                String model = scanner.nextLine();
                System.out.println("Введите цвет машины:");
                String color = scanner.nextLine();
                System.out.println("Введите регистрационный знак машины:");
                String number_of_registration = scanner.nextLine();
                System.out.println("Введите owner_id");
                Long owner_id = scanner.nextLong();
                Car car = Car.builder()
                        .model(model)
                        .color(color)
                        .number_of_registration(number_of_registration)
                        .owner_id(owner_id)
                        .build();

                carRepository.save(car);
                System.out.println("Закончили? Да/Нет");
                String exit = scanner.nextLine();
                if (exit.equals( "Да")){
                    break;
                }else {
                    System.out.println("Incorrect value of action");
                }
            }
        }


    }

}