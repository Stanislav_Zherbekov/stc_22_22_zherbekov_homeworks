package ru.inno.cars.repositiry;

import ru.inno.cars.models.Car;

import java.util.List;

public interface CarRepository {
     List<Car> findAll();
     void save (Car car);

}
