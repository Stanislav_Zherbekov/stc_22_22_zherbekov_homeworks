public class Main {
    public static void main(String[] args) {
        ArrayTask sumInRange = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }

            return sum;
        };

        ArrayTask sumNumberInMaxnumber = (array, from, to) -> {
            int maxnumber = array[from];
            for (int i = from; i <= to; i++) {
                if (array[i] > maxnumber) {
                    maxnumber = array[i];
                }
            }

            int number = maxnumber;
            int sum = 0;
            while (number > 0) {
                sum += number % 10;
                number /= 10;
            }

            return sum;

        };

        int[] array = {4, 8, 50, 435, 67, 12, 22};
        ArraysTasksResolver.resolveTask(array, sumInRange, 5, 6);
        ArraysTasksResolver.resolveTask(array, sumNumberInMaxnumber, 0, 6);


    }
}