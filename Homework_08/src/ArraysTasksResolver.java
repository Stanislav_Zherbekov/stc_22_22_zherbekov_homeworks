import java.util.Arrays;

public class ArraysTasksResolver {
    static void resolveTask(int[] array, ArrayTask task, int from, int to) {

        System.out.println("Исходный массив - " + Arrays.toString(array));
        System.out.println("Значения from - " + array[from] + ", " + "Значение to - " + array[to]);
        System.out.println(task.resolve(array, from, to));


    }

}
