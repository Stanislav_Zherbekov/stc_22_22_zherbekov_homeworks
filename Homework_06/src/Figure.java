public class Figure {
    protected double x, y;

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;

    }


    protected void moveFigure(double toX, double toY) {
        this.x = toX;
        this.y = toY;
        System.out.println("Новые координаты Figure: (" + getX() + "," + getY() + ").");
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
