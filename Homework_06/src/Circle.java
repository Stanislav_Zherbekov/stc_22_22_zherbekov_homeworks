public class Circle extends Figure {
    protected double radius;

    public Circle(double radius, double x, double y) {
        super(x, y);
        this.radius = radius;
    }

    public double calcPerimetr() {
        double perimetr;
        perimetr = 2 * Math.PI * radius;
        return perimetr;


    }

    public double calcArea() {
        double area;
        area = Math.PI * radius * radius;
        return area;
    }

    protected void moveFigure(double toX, double toY) {
        this.x = toX;
        this.y = toY;
        System.out.println("Новые координаты Circle: (" + getX() + "," + getY() + ").");
    }


}
