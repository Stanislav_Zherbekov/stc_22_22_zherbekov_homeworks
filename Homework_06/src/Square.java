public class Square extends Figure {


    protected double a;

    public Square(double a, double x, double y) {
        super(x, y);
        this.a = a;
    }

    public double calcPerimetr() {
        double perimetr;
        perimetr = 4 * a;
        return perimetr;

    }

    public double calcArea() {
        double area;
        area = a * a;
        return area;
    }

    protected void moveFigure(double toX, double toY) {
        this.x = toX;
        this.y = toY;
        System.out.println("Новые координаты Square: (" + getX() + "," + getY() + ").");
    }

}



