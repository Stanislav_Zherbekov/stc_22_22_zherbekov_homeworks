public class Main {
    public static void main(String[] args) {
        Figure figure = new Figure(2, 4);
        Elips elips = new Elips(2, 3, 1, 4);
        Circle circle = new Circle(3, 4, 5);
        Square square = new Square(2, 4, 5);
        Rectangle rectangle = new Rectangle(4, 6, 3, 5);
        square.calcPerimetr();
        rectangle.calcPerimetr();
        circle.calcPerimetr();
        elips.calcPerimetr();
        square.calcArea();
        rectangle.calcArea();
        circle.calcArea();
        elips.calcArea();
        square.moveFigure(4, 8);
        rectangle.moveFigure(5, 9);
        circle.moveFigure(3, 6);
        figure.moveFigure(5, 8);

    }

}