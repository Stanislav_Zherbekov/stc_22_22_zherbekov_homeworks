public class Elips extends Circle {

    private double maxradius;

    public Elips(double radius, double maxradius, double x, double y) {
        super(radius, x, y);

        this.maxradius = maxradius;
    }

    public double calcPerimetr() {
        double perimetr;
        perimetr = 4 * (Math.PI * radius * maxradius + (radius - maxradius)) / radius + maxradius;
        return perimetr;

    }

    public double calcArea() {
        double area;
        area = Math.PI * radius * maxradius;
        return area;
    }

    protected void moveFigure(double toX, double toY) {
        this.x = toX;
        this.y = toY;
        System.out.println("Новые координаты Elips: (" + getX() + "," + getY() + ").");
    }


}
