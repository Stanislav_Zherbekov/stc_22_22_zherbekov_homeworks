public class Rectangle extends Square {
    private double b;

    public Rectangle(double a, double b, double x, double y) {
        super(a, x, y);
        this.b = b;
    }

    public double calcPerimetr() {
        double perimetr;
        perimetr = (2 * (a + b));
        return perimetr;


    }

    public double calcArea() {
        double area;
        area = a * b;
        return area;

    }

    protected void moveFigure(double toX, double toY) {
        this.x = toX;
        this.y = toY;
        System.out.println("Новые координаты Rectangle: (" + getX() + "," + getY() + ").");
    }
}
