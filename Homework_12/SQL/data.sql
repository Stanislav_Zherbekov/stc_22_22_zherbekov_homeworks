insert into driver (first_name, last_name, phone_number, experience, age, driver_license, driver_license_category, rate)
values ('Иван', 'Иванов', 89213457869, 6, 24, true, 'A,B', 3),
       ('Сергей', 'Новиков', 89239484881, 3, 34, true, 'B,C', 2),
       ('Марк', 'Торохтий', 89217832465, 8, 28, true, 'B', 4),
       ('Кирилл', 'Жербеков', 89119901444, 12, 38, true, 'B', 5),
       ('Владислав', 'Аникеев', 89113456782, 5, 40, false, 'B,C', 1);

insert into car (model, color, number_of_registration, owner_id)
values ('Hunday Solaris', 'Black', 'Е092АР78', 1),
       ('Hunday Solaris', 'White', 'В122НМ178', 3),
       ('Kia Rio', 'White', 'В192НР177', 2),
       ('Mersedes-Benz GLK', 'Grey', 'С243АМ178', 4),
       ('Renult Logan', 'Blue', 'т872АС178', 4);

insert into trip (driver_id, car_id, date_of_trip, duration_of_the_trip)
values (1, 2, '2022-03-04', '3:00'),
       (3, 5, '2022-05-03', '7:00'),
       (2, 3, '2022-07-23', '5:40'),
       (4, 4, '2022-08-20', '6:30'),
       (1, 4, '2022-03-04', '3:00');