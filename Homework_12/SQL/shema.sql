create table driver
(
    id                      serial primary key,
    first_name              varchar(20)                               not null,
    last_name               varchar(20)                               not null,
    phone_number            varchar(11) unique                        not null,
    experience              integer check ( experience >= 0 and experience <= 42 ) default 0,
    age                     integer check (age >= 18 and age <= 60)   not null,
    driver_license          bool                                      not null,
    driver_license_category varchar(5)                                not null,
    rate                    integer check ( rate >= 0 and rate <= 5 ) not null
);
drop table driver;

create table car
(
    id                     serial primary key,
    model                  varchar(20)        not null,
    color                  varchar(10)        not null default 'Color',
    number_of_registration varchar(10) unique not null,
    owner_id               integer            not null,
    foreign key (owner_id) references driver (id)
);
create table trip
(
    id                   serial primary key,
    driver_id            integer not null,
    car_id               integer not null,
    date_of_trip         date,
    duration_of_the_trip time,
    foreign key (driver_id) references driver (id),
    foreign key (car_id) references car (id)

);
