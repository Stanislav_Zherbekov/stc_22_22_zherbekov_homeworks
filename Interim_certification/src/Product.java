public class Product {
    private Integer id;
    private String title;
    private double cost;
    private Integer countInStorage;


    public Product(Integer id, String title, double cost, Integer countInStorage) {
        this.id = id;
        this.title = title;
        this.cost = cost;
        this.countInStorage = countInStorage;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public double getCost() {
        return cost;
    }

    public Integer getCountInStorage() {
        return countInStorage;
    }


    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", cost=" + cost + '\'' + ", countInStorage=" + countInStorage +
                '}';
    }


}
