import java.io.*;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;



public class ProductsRepositoryFileBasedImpl implements ProductsRepostory {
    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        double cost = Double.parseDouble(parts[2]);
        Integer countInStorage = Integer.parseInt(parts[3]);
        return new Product(id, name, cost, countInStorage);
    };

    @Override
    public Product findById(Integer id) {
        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId() == id)
                    .min(Comparator.comparingInt(Product::getId))
                    .get();

        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }

    }


    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getTitle().contains(title))
                    .collect(Collectors.toList());


        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }

    }

    @Override
    public void update(Product product) {

    }
}